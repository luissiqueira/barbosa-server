#coding = utf-8
'''
Created on 19/06/2013

@author: luissiqueira
'''
from barbosa.util import get_file_path
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save

def get_sentinel_user():
    return User.objects.get_or_create(username='user_deleted')[0]

class UserBarbosa(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    fb_ip = models.CharField(_("facebook id"), max_length=100, blank=True)
    genero = models.CharField(_("gender"), max_length=10, blank=True)

def create_profile(sender, **kw):
    user = kw["instance"]
    if kw["created"]:
        ub = UserBarbosa(user=user)
        ub.save()
post_save.connect(create_profile, sender=User)

class Artista(models.Model):
    nome = models.CharField(_("name"), max_length=100)
    slug = models.CharField(_("slug"), max_length=100)
    salvo_em = models.DateTimeField(_("created in"), auto_now_add=True)
    excluido = models.BooleanField(_("excluded"))   

class Album(models.Model):
    artista = models.ForeignKey(Artista, verbose_name=_("artist"), blank=True)
    titulo = models.CharField(_("title"), max_length=100)
    slug = models.CharField(_("slug"), max_length=100)
    salvo_em = models.DateTimeField(_("created in"), auto_now_add=True)
    excluido = models.BooleanField(_("excluded"))

class Musica(models.Model):
    album = models.ForeignKey(Album, verbose_name=_("album"), blank=True)
    titulo = models.CharField(_("title"), max_length=100)
    arquivo = models.FileField(_("Arquivo"),upload_to=get_file_path)
    url = models.URLField(_("url"), blank=True)
    tempo = models.CharField(_("music time"), max_length=5, blank=True)
    salvo_em = models.DateTimeField(_("created in"), auto_now_add=True)
    excluida = models.BooleanField(_("excluded"))

class Lista(models.Model):
    criador = models.ForeignKey(User, verbose_name=_("list creator"), on_delete=models.SET(get_sentinel_user))
    titulo = models.CharField(_("title"), max_length=100)
    descricao = models.CharField(_("description"), max_length=100)
    criada_em = models.DateTimeField(_("created in"), auto_now_add=True)
    excluida = models.BooleanField(_("excluded"))

class MusicaLista(models.Model):
    lista = models.ForeignKey(Lista, verbose_name=_("playlist"), related_name="p+")
    musica = models.ForeignKey(Musica, verbose_name=_("music"), related_name="m+")
    ordem = models.IntegerField(_("order"), default=0)
    adicionado_por = models.ForeignKey(User, verbose_name=_("added by"), related_name="abu+", on_delete=models.SET(get_sentinel_user))
    adicionado_em = models.DateTimeField(_("added in"), auto_now_add=True)
    excluida = models.BooleanField(_("excluded"))
    
class UsuarioLista(models.Model):
    lista = models.ForeignKey(Lista, verbose_name=_("playlist"), related_name="p+")
    usuario = models.ForeignKey(User, verbose_name=_("user"), related_name="u+")
    permissao_adicionar_musica = models.BooleanField(_("permission to add music"))
    permissao_adicionar_usuario = models.BooleanField(_("permission to add user"))
    participacao_aprovada = models.BooleanField(_("approved"))
    adicionado_por = models.ForeignKey(User, verbose_name=_("added by"), related_name="abu+", on_delete=models.SET(get_sentinel_user))
    adicionado_em = models.DateTimeField(_("added in"), auto_now_add=True)
    excluido = models.BooleanField(_("excluded"))

class Evento(models.Model):
    criador = models.ForeignKey(User, verbose_name=_("event creator"), on_delete=models.SET(get_sentinel_user))
    titulo = models.CharField(_("title"), max_length=100)
    descricao = models.CharField(_("description"), max_length=100)
    criada_em = models.DateTimeField(_("created in"), auto_now_add=True)
    data = models.DateField(_("date"))
    hora = models.TimeField(_("hour"))
    excluido = models.BooleanField(_("excluded"))
    
class UsuarioEvento(models.Model):
    evento = models.ForeignKey(Evento, verbose_name=_("event"), related_name="e+")
    usuario = models.ForeignKey(User, verbose_name=_("user"), related_name="u+")
    permissao_adicionar_lista = models.BooleanField(_("permission to add music"))
    permissao_adicionar_usuario = models.BooleanField(_("permission to add user"))
    participacao_aprovada = models.BooleanField(_("approved"))
    adicionado_por = models.ForeignKey(User, verbose_name=_("added by"), related_name="abu+", on_delete=models.SET(get_sentinel_user))
    adicionado_em = models.DateTimeField(_("added in"), auto_now_add=True)
    excluido = models.BooleanField(_("excluded"))
    
class ListaEvento(models.Model):
    evento = models.ForeignKey(Evento, verbose_name=_("event"), related_name="e+")
    lista = models.ForeignKey(Lista, verbose_name=_("playlist"), related_name="p+")
    adicionado_por = models.ForeignKey(User, verbose_name=_("added by"), related_name="abu+", on_delete=models.SET(get_sentinel_user))
    adicionado_em = models.DateTimeField(_("added in"), auto_now_add=True)
    