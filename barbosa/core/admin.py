'''
Created on 20/06/2013

@author: luissiqueira
'''
from barbosa.core import models as barbosa_models
from django.contrib import admin
from django.db.models.base import ModelBase

for name, var in barbosa_models.__dict__.items():
    if type(var) is ModelBase and name != "User":
        admin.site.register(var)