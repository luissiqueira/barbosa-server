'''
Created on 25/06/2013

@author: luissiqueira
'''

import uuid, os

AUDIO_EXT_LIST = [ "mp3" ]

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    path = "uploads/"+str(instance.usuario.id)+"/"
    if ext in AUDIO_EXT_LIST:
        path += "musics"
    else:
        path += "default"
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(path, filename)